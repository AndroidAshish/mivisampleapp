package com.mivisample.ashish;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    EditText msnField;
    Button loginBtn;
    ProgressDialog progressDialog;
    String msn;
    String payment_type;
    String title;
    String first_name;
    String last_name;
    String date_of_birth;
    String contact_number;
    String email_address;
    String credit;
    String type;
    String included_data_balance;
    String expiry_date;
    String name;
    String price;
    String enteredMSN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        msnField = (EditText)findViewById(R.id.editText);
        loginBtn = (Button)findViewById(R.id.loginBtn);
        loadJSONFromAsset();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enteredMSN = msnField.getText().toString().trim();
                if(enteredMSN!=null && !enteredMSN.equals("")) {
                    new Login().execute();
                }else{
                    msnField.setError("Field should not be empty");
                    Toast.makeText(MainActivity.this, "Field should not be empty", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    private class Login extends AsyncTask<URL, Integer, Long> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Please Wait.....");
            progressDialog.setCancelable(false);
            progressDialog.show();

        }

        protected Long doInBackground(URL... urls) {
            try {
                JSONObject obj = new JSONObject(loadJSONFromAsset());
                JSONArray m_jArry = obj.getJSONArray("included");
                JSONObject m_jObject = obj.getJSONObject("data");
                JSONObject jo_mObject = m_jObject.getJSONObject("attributes");
                title = jo_mObject.getString("title");
                first_name = jo_mObject.getString("first-name");
                last_name = jo_mObject.getString("last-name");
                date_of_birth = jo_mObject.getString("date-of-birth");
                contact_number = jo_mObject.getString("contact-number");
                email_address = jo_mObject.getString("email-address");
                payment_type = jo_mObject.getString("payment-type");
                for (int i = 0; i < m_jArry.length(); i++) {
                    JSONObject jo_inside = m_jArry.getJSONObject(i);
                    JSONObject j_nested = jo_inside.getJSONObject("attributes");
                    if(j_nested.has("msn")) {
                        msn = j_nested.getString("msn");
                    }if(j_nested.has("credit")){
                        credit = j_nested.getString("credit");
                    }if(j_nested.has("included-data-balance")){
                        included_data_balance = j_nested.getString("included-data-balance");
                    }if(j_nested.has("expiry-date")){
                        expiry_date = j_nested.getString("expiry-date");
                    }if(j_nested.has("name")){
                        name = j_nested.getString("name");
                    }if(j_nested.has("price")){
                        price = j_nested.getString("price");
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Long result) {
            //0468874507
            if(enteredMSN.equals(msn)){
                progressDialog.dismiss();
                Intent i = new Intent(MainActivity.this, WelcomeActivity.class);
                i.putExtra("title",title);
                i.putExtra("first_name",first_name);
                i.putExtra("last_name",last_name);
                i.putExtra("date_of_birth",date_of_birth);
                i.putExtra("contact_number",contact_number);
                i.putExtra("email_address",email_address);
                i.putExtra("payment_type",payment_type);
                i.putExtra("included_data_balance",included_data_balance);
                i.putExtra("expiry_date",expiry_date);
                i.putExtra("name",name);
                i.putExtra("price",price);
                startActivity(i);
            }else{
                Toast.makeText(MainActivity.this, "Entered MSN is not correct", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }

        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("collection.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}