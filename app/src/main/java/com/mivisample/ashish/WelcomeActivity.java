package com.mivisample.ashish;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    TextView splash_msg;
    String msn;
    String payment_type;
    String title;
    String first_name;
    String last_name;
    String date_of_birth;
    String contact_number;
    String email_address;
    String credit;
    String type;
    String included_data_balance;
    String expiry_date;
    String name;
    String price;
    String enteredMSN;

    private static int SPLASH_TIME_OUT = 3000;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        splash_msg = (TextView)findViewById(R.id.splash_msg);
        Intent i = getIntent();
        if(i!=null) {
            splash_msg.setText("Welcome " + i.getStringExtra("title") + " "
                    + i.getStringExtra("first_name")
                    + " " + i.getStringExtra("last_name"));
            title = i.getStringExtra("title");
            first_name = i.getStringExtra("first_name");
            last_name = i.getStringExtra("last_name");
            date_of_birth = i.getStringExtra("date_of_birth");
            contact_number = i.getStringExtra("contact_number");
            email_address = i.getStringExtra("email_address");
            payment_type = i.getStringExtra("payment_type");
            included_data_balance = i.getStringExtra("included_data_balance");
            expiry_date = i.getStringExtra("expiry_date");
            name = i.getStringExtra("name");
            price = i.getStringExtra("price");

        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(WelcomeActivity.this, DetailsActivity.class);
                i.putExtra("title",title);
                i.putExtra("first_name",first_name);
                i.putExtra("last_name", last_name);
                i.putExtra("date_of_birth",date_of_birth);
                i.putExtra("contact_number",contact_number);
                i.putExtra("email_address",email_address);
                i.putExtra("payment_type",payment_type);
                i.putExtra("included_data_balance",included_data_balance);
                i.putExtra("expiry_date",expiry_date);
                i.putExtra("name",name);
                i.putExtra("price",price);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
