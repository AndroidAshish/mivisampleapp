package com.mivisample.ashish;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    TextView name_customer;
    TextView contact_number;
    TextView email_address;
    TextView date_of_birth;
    TextView payment_type;
    TextView included_data_balance;
    TextView expiry_date;
    TextView name;
    TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        name_customer = (TextView) findViewById(R.id.name_customer);
        contact_number = (TextView) findViewById(R.id.contact_number);
        email_address = (TextView) findViewById(R.id.email_address);
        date_of_birth = (TextView) findViewById(R.id.date_of_birth);
        payment_type = (TextView) findViewById(R.id.payment_type);
        expiry_date = (TextView) findViewById(R.id.expiry_date);
        name = (TextView) findViewById(R.id.name);
        price = (TextView) findViewById(R.id.price);
        included_data_balance = (TextView) findViewById(R.id.included_data_balance);
        Intent i = getIntent();
        if (i != null) {
            name_customer.setText("Name: " + i.getStringExtra("title") + " "
                    + i.getStringExtra("first_name")
                    + " " + i.getStringExtra("last_name"));
            contact_number.setText("Contact Number: "+i.getStringExtra("contact_number"));
            email_address.setText("Email Address: "+i.getStringExtra("email_address"));
            date_of_birth.setText("Date of birth: "+i.getStringExtra("date_of_birth"));
            payment_type.setText("Plan Type: "+i.getStringExtra("payment_type"));
            included_data_balance.setText("Data Balance: "+i.getStringExtra("included_data_balance"));
            expiry_date.setText("Expiry Date: "+i.getStringExtra("expiry_date"));
            name.setText("Product Name: "+i.getStringExtra("name"));
            price.setText("Price: "+i.getStringExtra("price"));
        }
    }
}
